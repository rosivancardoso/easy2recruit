$(".links-pagina").click(function(e) {
	e.preventDefault();
    let id = $(this).attr('href'), 
        targetOffset = $(id).offset().top;
    $('html,body').animate({ scrollTop: targetOffset}, 500);    
});

let componentes = { 
    imagens : document.getElementsByClassName('imagens') 
}   

function slideImagens(direcao){
    let destino = 0;
    let atual = 0;
    for(atual = 0; atual < componentes.imagens.length; atual++){
        if(componentes.imagens[atual].classList.contains('atual')){
            componentes.imagens[atual]. classList.remove('atual');            
            break;
        }
    }
    destino = atual;


    if(direcao === 'proximo' && atual < 7) {
        destino++;
    }else{
        if(direcao === 'anterior' && atual > 0 && atual < 8){
            destino--;
        }else{
            destino = atual === 0 ? 7 : 0;
        }        
    }
    componentes.imagens[destino]. classList.add('atual');
}

document.getElementById('anterior').onclick = () => slideImagens('anterior');
document.getElementById('proximo').onclick = () => slideImagens('proximo');
